FROm openjdk:8

WORKDIR /opt/app

ENV LOG_PATH "/logs"
ENV SERVER_PORT 8080

EXPOSE $SERVER_PORT

ARG JAR_FILE=target/recommendation-service.jar
ARG PROP_FILE=src/main/resources/application.properties

COPY ${JAR_FILE} app/recommendation-service.jar
COPY ${PROP_FILE} app/application.properties

ENTRYPOINT ["java","-jar","app/recommendation-service.jar"]
