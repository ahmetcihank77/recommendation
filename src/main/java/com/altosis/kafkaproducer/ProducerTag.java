package com.altosis.kafkaproducer;

import com.google.gson.JsonObject;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class ProducerTag implements Runnable {

    private KafkaProducer<String, String> producer = null;
    private volatile boolean exit = false;

    public ProducerTag(){
        Properties properties = new Properties();
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, IKafkaConstantsTag.KAFKA_BROKERS);
        properties.put(ProducerConfig.CLIENT_ID_CONFIG, IKafkaConstantsTag.CLIENT_ID);
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,StringSerializer.class.getName());
        producer = new KafkaProducer<String, String>(properties);
    }

    @Override
    public void run() {

        while (!exit){  //  appID  orgID

                       for(int i = 0; i<3; i++){
                        JsonObject jsonObject = new JsonObject();
                        jsonObject.addProperty("appID","testApp");
                        jsonObject.addProperty("orgID","testOrg");
                        jsonObject.addProperty("hashtag","testtag1");
                        jsonObject.addProperty("itemID","150");
                        ProducerRecord<String,String> producerRecord = new ProducerRecord<String, String>(IKafkaConstantsTag.TOPIC_NAME, jsonObject.toString());
                        producer.send(producerRecord);
                       }

            for(int i = 0; i<4; i++){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("appID","testApp");
                jsonObject.addProperty("orgID","testOrg");
                jsonObject.addProperty("hashtag","testtag2");
                jsonObject.addProperty("itemID","520");
                ProducerRecord<String,String> producerRecord = new ProducerRecord<String, String>(IKafkaConstantsTag.TOPIC_NAME, jsonObject.toString());
                producer.send(producerRecord);
            }
            for(int i = 0; i<5; i++){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("appID","testApp");
                jsonObject.addProperty("orgID","testOrg");
                jsonObject.addProperty("hashtag","testtag3");
                jsonObject.addProperty("itemID","1210");
                ProducerRecord<String,String> producerRecord = new ProducerRecord<String, String>(IKafkaConstantsTag.TOPIC_NAME, jsonObject.toString());
                producer.send(producerRecord);
            }
            for(int i = 0; i<6; i++){
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("appID","testApp");
                jsonObject.addProperty("orgID","testOrg");
                jsonObject.addProperty("hashtag","testtag4");
                jsonObject.addProperty("itemID","899");
                ProducerRecord<String,String> producerRecord = new ProducerRecord<String, String>(IKafkaConstantsTag.TOPIC_NAME, jsonObject.toString());
                producer.send(producerRecord);
            }
        }
    }
}
