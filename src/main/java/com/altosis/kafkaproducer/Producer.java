package com.altosis.kafkaproducer;

import com.google.gson.JsonObject;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;


import java.util.Properties;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;


public class Producer implements Runnable{

    private  KafkaProducer<String, String> producer = null;
    private volatile boolean exit = false;

    public Producer(){
        Properties properties = new Properties();
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, IKafkaConstants.KAFKA_BROKERS);
        properties.put(ProducerConfig.CLIENT_ID_CONFIG, IKafkaConstants.CLIENT_ID);
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,StringSerializer.class.getName());
        producer = new KafkaProducer<String, String>(properties);
    }



    @Override
    public void run() {

       while (!exit){  //  appID  orgID
            try{
               BufferedReader br = new BufferedReader(new FileReader("rating_rel.csv"));
                try {
                    String line = br.readLine();
                    StringBuilder sb = new StringBuilder();
                    while (line != null) {
                        String[] main = line.split(",");
                        String StringUser = main[0];
                        String StringMovie = main[1];
                        float rating = Float.parseFloat(main[2]);
                        line = br.readLine();
                        JsonObject jsonObject = new JsonObject();
                        jsonObject.addProperty("userId",main[0]);
                        jsonObject.addProperty("itemId",main[1]);
                        jsonObject.addProperty("hashtag","deneme");
                        jsonObject.addProperty("appID","testApp");
                        jsonObject.addProperty("orgID","testOrg");
                        jsonObject.addProperty("rating",main[2].toString());
                        jsonObject.addProperty("appID","testApp");
                        jsonObject.addProperty("orgID","testOrg");
                        jsonObject.addProperty("orgID","testOrg");
                        jsonObject.addProperty("orgID","testOrg");
                        jsonObject.addProperty("hashtag","deneme");
                        ProducerRecord<String,String> producerRecord = new ProducerRecord<String, String>(IKafkaConstants.TOPIC_NAME, jsonObject.toString());
                        producer.send(producerRecord);
                    }
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    try
                    {
                        br.close();
                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();
                    }
                }

            }catch (Exception exp){
                System.out.println("Error mate :)");
                System.out.println(exp.getCause());
            }

        }
    }

}
