package com.altosis.kafkaconsumer;

public interface KafkaConsumerConstants {

    public static  String KAFKA_BROKERS ="localhost:9092";
    public static  String  CLIENT_ID ="recommendation";
    public static  String TOPIC_NAME ="ztest";
    public static  String GROUP_ID_CONFIG ="consumergrup";
    public static  Integer MAX_NO_MESSAGE_FOUND_COUNT = 100;
    public static  String OFFSET_RESET_LATEST ="latest";
    public static  String OFFSET_RESET_EARLIER ="earliest";
    public static  Integer MAX_POLL_RECORDS =100;
    public static  Boolean ENABLE_AUTO_COMMIT_CONFIG = false;


}
