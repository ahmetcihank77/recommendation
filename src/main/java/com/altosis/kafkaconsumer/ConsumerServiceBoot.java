package com.altosis.kafkaconsumer;


import com.altosis.utils.DgrapUtility;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.protobuf.ByteString;
import io.dgraph.DgraphClient;
import io.dgraph.DgraphGrpc;
import io.dgraph.DgraphProto;
import io.dgraph.Transaction;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;


@Service
public class ConsumerServiceBoot {
   ManagedChannel channel = ManagedChannelBuilder.forAddress("172.17.0.1", 9080) // ip Address ve port App prop'tan okunmalı
            .usePlaintext().build();
    DgraphGrpc.DgraphStub stub = DgraphGrpc.newStub(channel);
    DgraphClient dgraphClient = new DgraphClient(stub);

    Gson gson = null;
    JsonParser parser = new JsonParser();

    public static String queryhtags;
    public static String mutationhtags;

    @Value("${interpolatedupserthtags}")
    public void setQueryhtags(String queryhtags){
        this.queryhtags = queryhtags;
    }

    @Value("${interpolatedmuthtags}")
    public void setMutationhtags(String mutationhtags){
        this.mutationhtags = mutationhtags;
    }

/*
    @KafkaListener(topics = "like", groupId = "dgraph", containerFactory = "dgraphKafkaListenerContainerFactory", concurrency = "1")
    public void connectorListener(String record) throws InterruptedException {
        DgrapUtility.doUpsertRating(record);
    }


    @KafkaListener(topics = "hashtag", groupId = "dgrapz",containerFactory = "dgraphKafkaListenerContainerFactoryTag", concurrency = "1")
    public void taglistener(String record) throws  InterruptedException{
      //  DgrapUtility.doUpsertTaging(record);
        doUpsertTag(record);
    }

*/
    private  void doUpsertTag(String record){

        Transaction txn = dgraphClient.newTransaction();
        JsonObject jsonObject = parser.parse(record).getAsJsonObject();
        String hashtag = jsonObject.get("hashtag").getAsString();
        hashtag = "\"" + hashtag + "\"";
        String itemID = jsonObject.get("itemID").getAsString();
        itemID = "\"" + itemID + "\"";
        String appID = jsonObject.get("appID").getAsString();
        appID = "\"" + appID + "\"";
        String orgID = jsonObject.get("orgID").getAsString();
        orgID = "\"" + orgID + "\"";
        String interpolatedqueryhtags = String.format(queryhtags, hashtag, itemID);
        String interpolatedmutationhtags = String.format(mutationhtags, hashtag, appID, orgID, itemID, appID, orgID);

        DgraphProto.Mutation mu = DgraphProto.Mutation.newBuilder().setSetNquads(ByteString.copyFromUtf8(interpolatedmutationhtags  )).build();
        DgraphProto.Request mreq = DgraphProto.Request.newBuilder().setQuery(interpolatedqueryhtags).addMutations(mu).setCommitNow(true).build();
        try{
            txn.doRequest(mreq);
        }

        catch(Exception ex){
            if(ex.getMessage().equals("java.util.concurrent.CompletionException: io.dgraph.TxnConflictException: Transaction has been aborted. Please retry")){
               // logger.warn(record);
                doUpsertTag(record);
            }
            else {
                //logger.error(ex.getMessage());
            }
        }
    }
}
