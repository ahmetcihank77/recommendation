package com.altosis;

import com.altosis.factorypattern.BaseFactory;
import com.altosis.factorypattern.CommandBase;
import com.altosis.factorypattern.FactoryCreator;
import com.altosis.factorypattern.MutationCreateImpl;
import com.altosis.kafkaconsumer.ConsumerService;
import com.altosis.kafkaproducer.IKafkaConstants;
import com.altosis.kafkaproducer.Producer;
import com.altosis.kafkaproducer.ProducerTag;
import com.google.gson.JsonObject;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Component;



@Component
public class AppStartComponent {

    @Value("${userschemestr}")
    public String userschemaStr;

    @Value("${blogschemestr}")
    public String blogschemestr;

    @Value("${userblogType}")
    public String userBlogType;

    @Autowired
    MutationCreateImpl mutationCreate;

    @Autowired
    private KafkaTemplate<String, String> sender;

    @EventListener
    public void onApplicationEvent(ContextRefreshedEvent event)  {
        //BaseFactory myfac = FactoryCreator.getFactory("Mutation");
        //CommandBase mutation = myfac.getCommand("CREATE");
        mutationCreate.createCommand(userBlogType);
        System.out.println("Rec app Starting");
/*
       ThreadPoolTaskScheduler scheduler = new ThreadPoolTaskScheduler();
        scheduler.setPoolSize(25);
        scheduler.initialize();

        for(int i = 0; i<25; i++){
            ConsumerService consumerService = new ConsumerService();
            scheduler.submit(consumerService);
        }

        Thread consumerThread = new Thread(consumerService);
        consumerThread.start();

*/
/*
        Producer kafkaProducer = new Producer();
        Thread worker = new Thread(kafkaProducer);
        worker.start();

        ProducerTag producerTag = new ProducerTag();
        Thread workertag = new Thread(producerTag);
        workertag.start();
*/


    }

}
