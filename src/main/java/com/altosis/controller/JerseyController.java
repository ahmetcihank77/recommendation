package com.altosis.controller;


import com.altosis.factorypattern.QueryImpl;
import com.altosis.utils.DgrapUtility;
import com.altosis.controller.models.PopularReqModel;
import com.altosis.controller.models.PostRatingModel;
import com.altosis.controller.models.RecommendReqModel;
import com.altosis.factorypattern.BaseFactory;
import com.altosis.factorypattern.CommandBase;
import com.altosis.factorypattern.FactoryCreator;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.protobuf.ByteString;
import io.dgraph.DgraphClient;
import io.dgraph.DgraphGrpc;
import io.dgraph.DgraphProto;
import io.dgraph.Transaction;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

/*
@Component
@Path("/")
@Consumes("application/json")
@Produces("application/json")*/
@RestController
public class JerseyController {

    private static final Logger logger = LoggerFactory.getLogger(JerseyController.class);


    @Autowired
    RestTemplate restTmplt;

    @Value("${reccommendquerystr}")
    public String recqueryStr;

    @Value("${recomstrwithapporg}")
    public String mrecqueryStr;

    @Value("${popularItems}")
    public String mpopularString;

    @Value("${recommendlimit}")
    public int  recommendLimit;

    @Value("${hashtagcounts}")
    public String hashtagquery;

    @Value("${ratingquery}")
    private String ratedQueryStr;

    @Value("${interpolatedmut}")
    private String mutation;

    @Value("${interpolatedupsert}")
    private String query;

    @Value("${dgraphIp}")
    String dgraphIp;

    @Value("${dgraphPort}")
    int dgraphPort;

    @Autowired
    QueryImpl queryImpl;

    @Autowired
    Gson gson;

    JsonParser parser = new JsonParser();

    static float exRating;

    //@POST
    //@Path("/recommend")
    @PostMapping("/recommend")
    public ResponseEntity getRecommendedItems(@RequestBody  RecommendReqModel recommendReqModel){
        String copyStr = mrecqueryStr;
        String copyPopularStr = mpopularString;
        copyPopularStr = copyPopularStr +",$appID:"+recommendReqModel.getAppID();
        copyPopularStr = copyPopularStr +",$orgID:"+recommendReqModel.getOrgID();

        copyStr = copyStr+ ",$userId:"+recommendReqModel.getUsername();
        copyStr = copyStr+ ",$appID:"+recommendReqModel.getAppID();
        copyStr = copyStr+ ",$orgID:"+recommendReqModel.getOrgID();

        System.out.println(copyStr);
        //BaseFactory qfucktory = FactoryCreator.getFactory("Query");
      //  CommandBase query = qfucktory.getCommand("createCommand");
        DgraphProto.Response response = queryImpl.createCommand(copyStr);
        String mm = response.getJson().toStringUtf8();
        System.out.println(response.getJson().toStringUtf8());
        JsonObject jsonObjectrecommend = parser.parse(response.getJson().toStringUtf8()).getAsJsonObject();
        JsonArray jsonArrayrecommend = jsonObjectrecommend.getAsJsonArray("Recommendation");

        int countRecommendation = jsonArrayrecommend.size();
        if(countRecommendation<recommendLimit){
            //BaseFactory pqfucktory = FactoryCreator.getFactory("Query");
            //CommandBase pquery = pqfucktory.getCommand("createCommand");
            DgraphProto.Response presponse = queryImpl.createCommand(copyPopularStr);//bura
            System.out.println(presponse.getJson().toStringUtf8());

            JsonObject jsonObjectPopular = parser.parse(presponse.getJson().toStringUtf8()).getAsJsonObject();
            JsonArray jsonArrayPopular = jsonObjectPopular.getAsJsonArray("partial");

            if(jsonArrayPopular.size()>0){
                for(int i =0; i<(jsonArrayPopular.size()-countRecommendation); i++){
                    JsonObject qjsonObj = new JsonObject();
                    qjsonObj.addProperty("itemId",jsonArrayPopular.get(i).getAsJsonObject().get("itemId").getAsString());
                    jsonArrayrecommend.add(qjsonObj);
                }
            }
        }

       /* JsonObject returnjsonObj = new JsonObject();
        returnjsonObj.addProperty("appID", recommendReqModel.getAppID());
        returnjsonObj.addProperty("orgID", recommendReqModel.getOrgID());
        returnjsonObj.addProperty("recommendations",jsonArrayrecommend.toString());

        String[] filterparam={"collectionName:blogs"};
        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_JSON);
        Map<String, String> bodyParamMap = new HashMap<String, String>();
        bodyParamMap.put("applicationId", "9a30857e-2d53-4022-a793-0c6f83370533");
        bodyParamMap.put("organizationId", "3b7db78d-35c0-48fd-b0b7-05339fa340be");
        bodyParamMap.put("relations", "false");
        bodyParamMap.put("query", DgrapUtility.IdConcetanator(jsonArrayrecommend));
        logger.info(jsonArrayrecommend.toString());

        ResponseEntity<String> result = restTmplt.postForEntity("http://localhost:8091/api/collection/query", bodyParamMap, String.class);
        JsonArray jsonArrayCollection = parser.parse(result.getBody()).getAsJsonObject().get("data").getAsJsonArray();
        JsonArray jsonArrayBlogname = new JsonArray();

        for(int i = 0; i<jsonArrayCollection.size(); i++){
            jsonArrayBlogname.add(jsonArrayCollection.get(i).getAsJsonObject().get("content"));
        }*/

        return  ResponseEntity.status(HttpStatus.OK).body(jsonArrayrecommend.toString());
    }

    //@POST
    //@Path("/populars")
    @PostMapping("/populars")
    public ResponseEntity getPopularItems(@RequestBody PopularReqModel popularReqModel){
        try {
            String copyPopularStr = mpopularString;
          //  copyPopularStr = copyPopularStr + ",$appID:" + popularReqModel.getAppID();
           // copyPopularStr = copyPopularStr + ",$orgID:" + popularReqModel.getOrgID();

            BaseFactory pqfucktory = FactoryCreator.getFactory("Query");
            CommandBase pquery = pqfucktory.getCommand("createCommand");
            DgraphProto.Response presponse = pquery.createCommand(copyPopularStr);
            System.out.println(presponse.getJson().toStringUtf8());

            JsonObject jsonObjectPopular = parser.parse(presponse.getJson().toStringUtf8()).getAsJsonObject();
            JsonArray jsonArrayPopular = jsonObjectPopular.getAsJsonArray("partial");
            return ResponseEntity.status(HttpStatus.OK).body(jsonArrayPopular.toString());
        }
        catch (Exception ex){
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("hata",ex.getCause().toString());
            return  ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(jsonObject.toString());
        }

    }

    //@POST
    //@Path("/hashtagcounts")
    @PostMapping("/hashtagcounts")
    public ResponseEntity getHashtagCounts(@RequestBody  PopularReqModel popularReqModel){
        try {
            String copyPopularStr = hashtagquery;
            //copyPopularStr = copyPopularStr + ",$appID:" + popularReqModel.getAppID();
           // copyPopularStr = copyPopularStr + ",$orgID:" + popularReqModel.getOrgID();

            BaseFactory pqfucktory = FactoryCreator.getFactory("Query");
            CommandBase pquery = pqfucktory.getCommand("createCommand");
            DgraphProto.Response presponse = pquery.createCommand(copyPopularStr);
            //String pmm = response.getJson().toStringUtf8();
            System.out.println(presponse.getJson().toStringUtf8());

            JsonObject jsonObjectPopular = parser.parse(presponse.getJson().toStringUtf8()).getAsJsonObject();
            JsonArray jsonArrayPopularht = jsonObjectPopular.getAsJsonArray("partial");
            logger.info(jsonArrayPopularht.toString());
            return ResponseEntity.status(HttpStatus.OK).body(jsonArrayPopularht.toString());
        }
        catch (Exception ex){
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("hata",ex.getCause().toString());
            return  ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ex.getCause().toString());
        }
    }

    @PostMapping("/postrating")
    public ResponseEntity postRating(@RequestBody PostRatingModel postRatingModel) {

        ManagedChannel channel = ManagedChannelBuilder.forAddress(dgraphIp, dgraphPort) // ip Address ve port App prop'tan okunmalı
                .usePlaintext().build();
        DgraphGrpc.DgraphStub stub = DgraphGrpc.newStub(channel);
        DgraphClient dgraphClient  = new DgraphClient(stub);



        Transaction txn = dgraphClient.newTransaction();

        String userId = postRatingModel.getUserID();
        userId = "\"" + userId + "\"";
        String blogId = postRatingModel.getItemID();
        blogId = "\"" + blogId + "\"";
        String appID = postRatingModel.getAppID();
        appID = "\"" + appID + "\"";
        String orgID = postRatingModel.getOrgID();
        orgID = "\"" + orgID + "\"";
        float rating = 0;
        String ratingStr = postRatingModel.getRating();
        //  String ratingStr = "";

        /*Comment Sayısı Alınacak*/
        if (ratingStr.equalsIgnoreCase("LIKE"))
            rating = 5;
        else if (ratingStr.equalsIgnoreCase("VIEW"))
            rating = 3.01f;
        else if (ratingStr.equalsIgnoreCase("DISLIKE"))
            rating = 0f;
        else if (ratingStr.equalsIgnoreCase("COMMENT"))
            rating = 3.7f;
        else if (ratingStr.equalsIgnoreCase("REMOVECOMMENT"))
            rating = 0f;


        String copyRatedStr = ratedQueryStr + ",$userId:" + postRatingModel.getUserID() + ",$itemId:" + postRatingModel.getItemID();
        //BaseFactory qfucktory = FactoryCreator.getFactory("Query");
       // CommandBase ratedquery = qfucktory.getCommand("createCommand");
        DgraphProto.Response response = queryImpl.createCommand(copyRatedStr);
        JsonObject jsonObjectrating = parser.parse(response.getJson().toStringUtf8()).getAsJsonObject();
        JsonArray jsonArrayrating = jsonObjectrating.getAsJsonArray("data");
        JsonObject ratingvalObj = new JsonObject();
        if (jsonArrayrating.size() > 0) ratingvalObj = jsonArrayrating.get(0).getAsJsonObject();
        if (ratingvalObj.size() > 1) {
            exRating = ratingvalObj.get("rated").getAsJsonArray().get(0).getAsJsonObject().get("rated|rating").getAsFloat();
            if (exRating == 5f && ratingStr.equalsIgnoreCase("VIEW"))
                rating = 5f;
            else if (exRating == 0f && ratingStr.equalsIgnoreCase("VIEW"))
                rating = 3.01f;
            else if (exRating == 3.7f && ratingStr.equalsIgnoreCase("REMOVECOMMENT"))
                rating = 0f;
            else if (exRating == 0f && ratingStr.equalsIgnoreCase("COMMENT"))
                rating = 3.7f;
            System.out.println(exRating);
        }
        System.out.println(response.getJson().toStringUtf8());
        String interpolatedquery = String.format(query, userId, blogId);
        String interpolatedmutation = String.format(mutation, userId, appID, orgID, blogId, appID, orgID, rating);
        DgraphProto.Mutation mu = DgraphProto.Mutation.newBuilder().setSetNquads(ByteString.copyFromUtf8(interpolatedmutation)).build();
        DgraphProto.Request mreq = DgraphProto.Request.newBuilder().setQuery(interpolatedquery).addMutations(mu).setCommitNow(true).build();

        try {
            txn.doRequest(mreq);
        } catch (Exception ex) {
            if (ex.getMessage().equals("java.util.concurrent.CompletionException: io.dgraph.TxnConflictException: Transaction has been aborted. Please retry")) {

                postRating(postRatingModel);
            } else {
                logger.error(ex.getMessage());
            }
        }
       return ResponseEntity.status(HttpStatus.OK).body("upsert done");
    }

    /*
    @PostMapping("/postrating")
    public ResponseEntity postHashtag(){
        return  null;

    }
*/

}

