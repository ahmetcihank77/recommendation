package com.altosis.controller.models;

public class PopularReqModel {
    private String orgID;
    private String appID;

    public PopularReqModel(String orgID, String appID) {
        this.orgID = orgID;
        this.appID = appID;
    }

    public  PopularReqModel(){

    }

    public String getOrgID() {
        return orgID;
    }

    public void setOrgID(String orgID) {
        this.orgID = orgID;
    }

    public String getAppID() {
        return appID;
    }

    public void setAppID(String appID) {
        this.appID = appID;
    }
}
