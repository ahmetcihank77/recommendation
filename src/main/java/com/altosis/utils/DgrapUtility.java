package com.altosis.utils;

import com.altosis.factorypattern.BaseFactory;
import com.altosis.factorypattern.CommandBase;
import com.altosis.factorypattern.FactoryCreator;
import com.altosis.kafkaconsumer.ConsumerService;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.protobuf.ByteString;
import io.dgraph.DgraphClient;
import io.dgraph.DgraphGrpc;
import io.dgraph.DgraphProto;
import io.dgraph.Transaction;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


@Component
public class DgrapUtility {
    private static final Logger logger = LoggerFactory.getLogger(ConsumerService.class);

    public static String query;
    public static String mutation;
    public static String queryhtags;
    public static String mutationhtags;
    public static String ratedQueryStr;
    public static String host;
    public static int port;


    @Value("${interpolatedupsert}")
    public void setQuery(String query){
        this.query = query;
    }

    @Value("${interpolatedmut}")
    public void setMutation(String mutation){
        this.mutation = mutation;
    }

    @Value("${interpolatedupserthtags}")
    public void setQueryhtags(String queryhtags){
        this.queryhtags = queryhtags;
    }

    @Value("${interpolatedmuthtags}")
    public void setMutationhtags(String mutationhtags){
        this.mutationhtags = mutationhtags;
    }

    @Value("${ratingquery}")
    public void setRatedQueryStr(String ratedQueryStr){ this.ratedQueryStr = ratedQueryStr; }




    @Autowired
    Gson gson;

    static  ManagedChannel channel = ManagedChannelBuilder.forAddress("172.17.0.1", 9080) // ip Address ve port App prop'tan okunmalı
            .usePlaintext().build();
    static DgraphGrpc.DgraphStub stub = DgraphGrpc.newStub(channel);
    static DgraphClient dgraphClient  = new DgraphClient(stub);
    static  JsonParser parser = new JsonParser();
    static float exRating;

       public  static void doUpsertRating(String record){
        Transaction txn = dgraphClient.newTransaction();
        JsonObject jsonObject = parser.parse(record).getAsJsonObject();
        String userId = jsonObject.get("userId").getAsString();
        userId = "\"" + userId + "\"";
        String blogId = jsonObject.get("itemId").getAsString();
        blogId = "\"" + blogId + "\"";
        String appID = jsonObject.get("appID").getAsString();
        appID = "\"" + appID + "\"";
        String orgID = jsonObject.get("orgID").getAsString();
        orgID = "\"" + orgID + "\"";
        //float rating = jsonObject.get("rating").getAsFloat();
           float rating = 0;
        String ratingStr = jsonObject.get("rating").getAsString();
        //  String ratingStr = "";

      if(ratingStr.equalsIgnoreCase("LIKE"))
          rating = 5;
      else if(ratingStr.equalsIgnoreCase("VIEW"))
          rating = 3.01f;
      else if(ratingStr.equalsIgnoreCase("DISLIKE"))
          rating = 0f;

        String copyRatedStr = ratedQueryStr + ",$userId:"+jsonObject.get("userId").getAsString() +  ",$itemId:" +jsonObject.get("itemId").getAsString();
        BaseFactory qfucktory = FactoryCreator.getFactory("Query");
        CommandBase ratedquery = qfucktory.getCommand("createCommand");
        DgraphProto.Response response = ratedquery.createCommand(copyRatedStr);
        JsonObject jsonObjectrating = parser.parse(response.getJson().toStringUtf8()).getAsJsonObject();
        JsonArray jsonArrayrating = jsonObjectrating.getAsJsonArray("data");
        JsonObject ratingvalObj = new JsonObject();
       if(jsonArrayrating.size()>0)  ratingvalObj = jsonArrayrating.get(0).getAsJsonObject();
        if(ratingvalObj.size()>1){
             exRating = ratingvalObj.get("rated|rating").getAsJsonObject().get("0").getAsFloat();
             if(exRating ==5f && ratingStr.equalsIgnoreCase("VIEW"))
                 rating = 5f;
             else if(exRating == 0f && ratingStr.equalsIgnoreCase("VIEW"))
                 rating = 3.01f;
            System.out.println(exRating);
        }
        System.out.println(response.getJson().toStringUtf8());
        String interpolatedquery = String.format(query, userId, blogId);
        String interpolatedmutation = String.format(mutation ,userId, appID,orgID, blogId, appID, orgID, rating);
        DgraphProto.Mutation mu = DgraphProto.Mutation.newBuilder().setSetNquads(ByteString.copyFromUtf8(interpolatedmutation)).build();
        DgraphProto.Request mreq = DgraphProto.Request.newBuilder().setQuery(interpolatedquery).addMutations(mu).setCommitNow(true).build();

        try{
             txn.doRequest(mreq);
        }
        catch(Exception ex){
            if(ex.getMessage().equals("java.util.concurrent.CompletionException: io.dgraph.TxnConflictException: Transaction has been aborted. Please retry")){
                logger.warn(record);
                doUpsertRating(record);
            }
            else {
                logger.error(ex.getMessage());
            }
       }
    }

    public  static void doUpsertTaging(String record){

             Transaction txn = dgraphClient.newTransaction();
             JsonObject jsonObject = parser.parse(record).getAsJsonObject();
             String hashtag = jsonObject.get("hashtag").getAsString();
             hashtag = "\"" + hashtag + "\"";
             String blogId = jsonObject.get("itemID").getAsString();
             blogId = "\"" + blogId + "\"";
             String appID = jsonObject.get("appID").getAsString();
             appID = "\"" + appID + "\"";
             String orgID = jsonObject.get("orgID").getAsString();
             orgID = "\"" + orgID + "\"";


             String interpolatedqueryhtags = String.format(queryhtags, hashtag, blogId);
             String interpolatedmutationhtags = String.format(mutationhtags, hashtag, appID, orgID, blogId, appID, orgID);

             DgraphProto.Mutation mu = DgraphProto.Mutation.newBuilder().setSetNquads(ByteString.copyFromUtf8(interpolatedqueryhtags)).build();
             DgraphProto.Request mreq = DgraphProto.Request.newBuilder().setQuery(interpolatedmutationhtags).addMutations(mu).setCommitNow(true).build();
         try{
             txn.doRequest(mreq);
         }

         catch(Exception ex){  
            if(ex.getMessage().equals("java.util.concurrent.CompletionException: io.dgraph.TxnConflictException: Transaction has been aborted. Please retry")){
                logger.warn(record);
                doUpsertTaging(record);
            }
            else {
                logger.error(ex.getMessage());
            }
        }
    }

    public static double sigmoid(double x) {
        return (1/( 1 + Math.pow(Math.E,(-1*x))));
    }

    public static double reversesigmoid(double y){
      return   Math.log(y/(1-y));
    }

    public static String IdConcetanator(JsonArray jsonArray){
        String idStr = "id:(";

        for(int i = 0; i<jsonArray.size(); i++){
            JsonObject jsonObject = jsonArray.get(i).getAsJsonObject();
            String mid =jsonObject.get("itemId").getAsString();

            if(i<jsonArray.size()-1)
               idStr =idStr+ "*"+mid+" , ";
            else if (i==jsonArray.size()-1){
                idStr =idStr+"*"+mid;
                idStr =idStr+") AND collectionName:\"blogs\"";
            }

        }
        idStr = idStr;
        return idStr;
    }

}
