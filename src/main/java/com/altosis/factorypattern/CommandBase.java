package com.altosis.factorypattern;

import io.dgraph.DgraphProto;

public interface CommandBase {
    public DgraphProto.Response createCommand(String commandStr);
}
