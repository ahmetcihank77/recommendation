package com.altosis.factorypattern;

public abstract class BaseFactory {
    public  abstract CommandBase  getCommand(String commandType);
}
