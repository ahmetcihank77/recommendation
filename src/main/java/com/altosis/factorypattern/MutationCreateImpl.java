package com.altosis.factorypattern;

import io.dgraph.DgraphClient;
import io.dgraph.DgraphGrpc;
import io.dgraph.DgraphProto;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class MutationCreateImpl implements CommandBase{

    @Value("${dgraphIp}")
    String dgraphIp;

    @Value("${dgraphPort}")
    int dgraphPort;

    @Override
    public DgraphProto.Response createCommand(String commandStr) {
        System.out.println("Hi from Mutuation Create Command ");
        ManagedChannel channel1 = ManagedChannelBuilder
                .forAddress(dgraphIp, dgraphPort)
                .usePlaintext().build();
        DgraphGrpc.DgraphStub stub1 = DgraphGrpc.newStub(channel1);

        DgraphClient dgraphClient = new DgraphClient(stub1);

        String schema = commandStr;
        DgraphProto.Operation operation = DgraphProto.Operation.newBuilder().setSchema(schema).build();
        dgraphClient.alter(operation);
        channel1.shutdown();
        return null;
    }
}
