package com.altosis.factorypattern;

public class MutationFactory extends BaseFactory {

    @Override
    public CommandBase getCommand(String commandType){
        if(commandType.equalsIgnoreCase("CREATE"))
            return new MutationCreateImpl();
        else if(commandType.equalsIgnoreCase("UPDATE"))
            return new MutationUpdateImpl();
        else
            return null;
    }

}
