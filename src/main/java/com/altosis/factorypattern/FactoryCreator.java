package com.altosis.factorypattern;

public class FactoryCreator {
    public  static  BaseFactory getFactory(String factoryType)
    {
        if(factoryType.equalsIgnoreCase("Mutation"))
            return new MutationFactory();
        else if(factoryType.equalsIgnoreCase("Query"))
            return  new QueryFactory();
        else
            return null;
    }
}
