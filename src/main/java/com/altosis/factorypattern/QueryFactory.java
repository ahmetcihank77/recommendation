package com.altosis.factorypattern;

public class QueryFactory  extends BaseFactory{
    @Override
    public CommandBase getCommand(String commandType){
        if(commandType.equalsIgnoreCase("createCommand")) return new QueryImpl();
        else return  null;
    }
}