package com.altosis.factorypattern;

import io.dgraph.DgraphClient;
import io.dgraph.DgraphGrpc;
import io.dgraph.DgraphProto;
import io.dgraph.Transaction;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.HashMap;
import java.util.stream.Collectors;

@Component
public class QueryImpl implements CommandBase  {

    @Value("${dgraphIp}")
    String dgraphIp;

    @Value("${dgraphPort}")
    int dgraphPort;


    @Override
    public DgraphProto.Response createCommand(String commandStr) {

        ManagedChannel channel1 = ManagedChannelBuilder.forAddress(dgraphIp, dgraphPort).usePlaintext().build();
        DgraphGrpc.DgraphStub stub1 = DgraphGrpc.newStub(channel1);
        DgraphClient dgraphClient = new DgraphClient(stub1);
        Transaction txn =null;
        DgraphProto.Response response;

        String[] mainQueryArray = commandStr.split(":::");
        String query1 = mainQueryArray[0];

        if (mainQueryArray.length > 1) {
            String[] params = Arrays.asList(mainQueryArray[1].split(",")).stream().filter(str->!str.isEmpty()).collect(Collectors.toList()).toArray(new String[0]);
            HashMap<String, String> mparams= new HashMap<String, String>();
            for (int i = 0; i < params.length; i++) {
                String[] keyValue = params[i].split(":");
                mparams.put(keyValue[0], keyValue[1]);
            }
            response = dgraphClient.newReadOnlyTransaction().queryWithVars(query1, mparams);
        } else
            response = dgraphClient.newReadOnlyTransaction().query(query1);
        //System.out.println(response.getJson().toStringUtf8());
        channel1.shutdown();

        return response;
    }
}
